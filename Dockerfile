FROM php:7.4-fpm

RUN apt-get update
RUN apt-get install -y \
        unzip \
        nano sudo git 

RUN docker-php-ext-enable opcache
RUN docker-php-ext-install calendar
RUN docker-php-ext-install bcmath
RUN docker-php-ext-install pdo_mysql
RUN docker-php-ext-install tokenizer

RUN apt-get install -y \
        libonig-dev \
    && docker-php-ext-install iconv mbstring

RUN apt-get install -y \
        libcurl4-openssl-dev \
    && docker-php-ext-install curl

RUN apt-get install -y \
        libssl-dev \
    && docker-php-ext-install ftp phar

RUN apt-get install -y \
        libicu-dev \
    && docker-php-ext-install intl

RUN apt-get install -y \
        libmcrypt-dev \
    && docker-php-ext-install session


RUN apt-get install -y \
        libzip-dev \
        zlib1g-dev \
    && docker-php-ext-install zip

RUN apt-get install -y \
        gettext \
    && docker-php-ext-install gettext



ENV COMPOSER_BINARY=/usr/local/bin/composer  \
    COMPOSER_HOME=/usr/local/composer
ENV PATH $PATH:$COMPOSER_HOME

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --ignore-platform-reqs --filename=composer


COPY php.fpm.ini /etc/php7.4/fpm/php.ini
COPY php.cli.ini /etc/php7.4/cli/php.ini

RUN apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# Add user for laravel application
RUN groupadd -g 99999 www
RUN useradd -u 99999 -ms /bin/bash -g www www --password=123456 | chpasswd && adduser www sudo

WORKDIR /var/www
# Copy existing application directory contents

RUN chown -R www .

# Copy existing application directory permissions
COPY --chown=www:www . /var/www

# Change current user to www
USER www


EXPOSE 9000

CMD ["php-fpm", "-F", "-c", "/etc/php8/fpm"]
